const auth = require('../auth');
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
// const Product = require('../models/productModel');
const Item = require('../models/itemModel');
const mongoose = require('mongoose');
const Tray = require('../models/trayModel');

// check email for registration
module.exports.checkEmailExists = async (request, response, next) => {
  return User.find({ email: request.body.email }).then((result) => {
    let message = ``;
    if (result.length > 0) {
      message = `The ${request.body.email} is already taken, please use other email.`;
      return response.json({ message: message, status: false });
    } else {
      next();
    }
  });
};

// register
module.exports.registerUser = async (request, response) => {
  const { firstName, lastName, email, password, isAdmin, mobileNo } =
    request.body;
  // const item = new Item({
  //   _id: new mongoose.Types.ObjectId(),
  // });
  // item.save((error, tray) => {
  //   if (error) {
  //     console.log(error);
  //   } else {
  const tray = new Tray({
    _id: new mongoose.Types.ObjectId(),
    // items: [item._id],
  });
  tray.save((error, tray) => {
    if (error) {
      console.log(error);
    } else {
      const newUser = new User({
        firstName,
        lastName,
        email,
        //bcrypt.hashSync(<variable> , <salt rounds>)
        password: bcrypt.hashSync(password, 10),
        isAdmin,
        mobileNo,
        trayId: tray._id,
      });
      return newUser
        .save()
        .then((user) => {
          console.log(user);
          let token = auth.createAccessToken(user);
          response.json({
            message: `Congratulations, Sir/Ma'am ${newUser.firstName}!. You are now registered.`,
            status: true,
            accessToken: token,
          });
        })
        .catch((error) => {
          console.log(error);
          response.json({
            message: `Sorry ${newUser.firstName}, there was an error during the registration. Please try again.`,
            status: false,
          });
        });
    }
  });
};

// Get Profile Details
module.exports.profileDetails = (request, response) => {
  // user will be object that contains the id and email of the user that is currently logged in.
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  return User.findById(userData.id)
    .then((result) => {
      result.password = '**********';
      return response.send(result);
    })
    .catch((err) => {
      return response.send(err);
    });
};
//   });
// };

// creating a tray
// module.exports.createTray = async (request, response, next) => {
//   let newTray = new Order({})
//     .save()
//     .then((tray) => {
//       console.log(tray);
//       response.send(`Congratulations, Tray created.`);
//     })
//     .catch((error) => {
//       console.log(error);
//       response.send(
//         `Sorry, there was an error during the registration. Please try again.`
//       );
//     });

//   if (newTray) {
//     next();
//   } else {
//     response.send(`Fail to create Tray`);
//   }
// };

// login user
module.exports.loginUser = (request, response) => {
  // The findOne method, returns the first record in the collection that matches the search criteria.

  return User.findOne({ email: request.body.email }).then((result) => {
    console.log(result);

    if (result === null) {
      return response.send({ accessToken: 'empty' });
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        let token = auth.createAccessToken(result);
        console.log(token);
        return response.send({ accessToken: token });
      } else {
        return response.send({ accessToken: 'empty' });
      }
    }
  });
};

// update user role
module.exports.updateRole = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const { userIdToUpdate } = request.params;
  if (userData.isAdmin) {
    return User.findById(userIdToUpdate)
      .then((result) => {
        result.isAdmin = !result.isAdmin;
        return User.findByIdAndUpdate(userIdToUpdate, result, {
          new: true,
        })
          .then((document) => {
            document.password = '';
            response.send(document);
          })
          .catch((error) => response.send(error));
      })
      .catch((error) => response.send(error));
  } else {
    response.send(`You don't have access on this page!`);
  }
};

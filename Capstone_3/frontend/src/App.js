import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

import { useState, useEffect } from 'react';

import Home from './pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from './context/UserContext';

function App() {
  const [user, setUser] = useState({ id: null, isAdmin: false });
  const unSetUser = () => {
    localStorage.removeItem('accessToken');
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({ id: data._id, isAdmin: data.isAdmin });
      });
  }, []);
  return (
    <div className="App">
      <UserProvider value={{ user, setUser, unSetUser }}>
        <Router>
          <div className="pages">
            <Routes>
              <Route path="/" element={<Home />}></Route>
            </Routes>
          </div>
        </Router>
      </UserProvider>
    </div>
  );
}

export default App;

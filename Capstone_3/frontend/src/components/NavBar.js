import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Offcanvas from 'react-bootstrap/Offcanvas';
import './css/NavBar.css';
import LoginModal from './Modals/LoginModal';
import RegisterModal from './Modals/RegisterModal';

function NavBar() {
  return (
    <>
      <Navbar key="xl" bg="light" expand="xl" className="">
        <Container>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-xl`} />
          <Navbar.Brand href="#home" className="d-xl-none">
            React-Bootstrap
          </Navbar.Brand>

          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-xl`}
            aria-labelledby={`offcanvasNavbarLabel-expand-xl`}
            placement="start"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-xl`}>
                React-Bootstrap
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-around flex-grow-1 pe-3">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Shop</Nav.Link>
                <Nav.Link href="#link">About Us</Nav.Link>
                <Nav.Link href="#link">FAQs</Nav.Link>
                <Navbar.Brand href="#home" className="d-none d-xl-block">
                  React-Bootstrap
                </Navbar.Brand>
                <Nav.Link href="#link">Blogs</Nav.Link>
                <Nav.Link href="#link">Contact</Nav.Link>
                <NavDropdown title="Acoount" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.2">
                    <LoginModal />
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">
                    <RegisterModal />
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">Cart</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="#link" className="d-none d-xl-block">
                  Cart
                </Nav.Link>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
          <Nav.Link href="#link" className="d-xl-none">
            Cart
          </Nav.Link>
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;

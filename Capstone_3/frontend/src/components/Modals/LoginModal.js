import React, { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../context/UserContext';

function LoginModal() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disable, setDisable] = useState(true);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const { setUser } = useContext(UserContext);

  useEffect(() => {
    if (email && password) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [email, password]);

  const login = (event) => {
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.accessToken !== 'empty') {
          localStorage.setItem('accessToken', data.accessToken);
          retreiveUserDetails(data.accessToken);
          handleClose();
          Swal.fire({
            title: '<strong>Login Successfully!</strong>',
            html: '<i>Welcome to Homepage</i>',
            icon: 'success',
          });
        } else {
          console.log(`Wrong Credentials`);
          Swal.fire({
            title: '<strong>Login Failed!</strong>',
            html: '<i>Try Again!</i>',
            icon: 'error',
          });
          setPassword('');
        }
      });

    const retreiveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          setUser({ id: data._id, isAdmin: data.isAdmin });
        });
    };
  };

  return (
    <>
      <p onClick={handleShow}>Login </p>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={login} className="col-12">
            <Form.Group className="" controlId="formEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="" controlId="formPassword">
              <Form.Label>Enter Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mt-3" controlId="formCheckbox"></Form.Group>

            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" type="submit" disabled={disable}>
                Login
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}
export default LoginModal;

import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../context/UserContext';

import { Navigate } from 'react-router-dom';

function RegisterModal() {
  const [show, setShow] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setmobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [disable, setDisable] = useState(true);

  const [firstNameValidation, setFirstNameValidation] = useState('invalid');
  const [lastNameValidation, setLastNameValidation] = useState('invalid');
  const [mobileNoValidation, setMobileNoValidation] = useState('invalid');

  const { setUser } = useContext(UserContext);

  const history = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (
      firstName.length &&
      lastName.length &&
      mobileNo.length &&
      email &&
      password &&
      rePassword &&
      password === rePassword
    ) {
      setDisable(false);

      // alert('Successfully Registered');

      // setDisable(true);
      // alert('Password Not Match!');
    } else {
      setDisable(true);
    }
  }, [firstName, lastName, mobileNo, email, password, rePassword]);

  const registerHandler = (event) => {
    event.preventDefault();
    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password,
        mobileNo,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.status) {
          localStorage.setItem('accessToken', data.accessToken);
          retreiveUserDetails(data.accessToken);
          Swal.fire({
            title: 'Registered Successfully!',
            icon: 'success',
            text: data.message,
          });
          history('/');
        } else {
          Swal.fire({
            title: 'Registration Failed!',
            icon: 'error',
            text: data.message,
          });
          history('/');
        }
      });
  };

  const retreiveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({ id: data._id, isAdmin: data.isAdmin });
      });
  };

  return (
    <>
      <p onClick={handleShow}>Register</p>

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={registerHandler}
            className="col-12 p-3 was-validated "
          >
            <Form.Group className="mb-3 d-flex flex-column  flex-md-row align-items-md-center justify-content-md-around">
              <Form.Label className="col-md-5 col-12">
                First Name
                <Form.Control
                  type="text"
                  placeholder="Enter Last Name"
                  value={firstName}
                  onChange={(event) => setFirstName(event.target.value)}
                  required
                />
                {firstNameValidation === 'invalid' ? (
                  <div className="invalid-feedback">
                    Must be Atleat 11 Characters!
                  </div>
                ) : (
                  <>
                    <div className="valid-feedback">Looking Good !</div>
                  </>
                )}
              </Form.Label>

              <Form.Label className="col-md-5 col-12">
                Last Name
                <Form.Control
                  type="text"
                  placeholder="Enter Last Name"
                  value={lastName}
                  onChange={(event) => setLastName(event.target.value)}
                  required
                />
                {lastNameValidation === 'invalid' ? (
                  <div className="invalid-feedback">
                    Must be Atleat 11 Characters!
                  </div>
                ) : (
                  <>
                    <div className="valid-feedback">Looking Good !</div>
                  </>
                )}
              </Form.Label>
            </Form.Group>
            <Form.Group className="mb-3 d-flex flex-column flex-md-row align-items-md-center justify-content-md-around">
              <Form.Label className="col-md-5 col-12">
                Email address
                <Form.Control
                  className="me-3 "
                  type="email"
                  placeholder="Enter Email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                  required
                />
                {mobileNoValidation === 'invalid' ? (
                  <div className="invalid-feedback">
                    Must be Atleat 11 Characters!
                  </div>
                ) : (
                  <>
                    <div className="valid-feedback">Looking Good !</div>
                  </>
                )}
              </Form.Label>

              <Form.Label className="col-md-5 col-12">
                Mobile Number
                <Form.Control
                  type="number"
                  placeholder="Enter Mobile Number"
                  value={mobileNo}
                  onChange={(event) => setmobileNo(event.target.value)}
                  required
                />
                {mobileNoValidation === 'invalid' ? (
                  <div className="invalid-feedback">
                    Must be Atleat 11 Characters!
                  </div>
                ) : (
                  <>
                    <div className="valid-feedback">Looking Good !</div>
                  </>
                )}
              </Form.Label>
            </Form.Group>
            <div className="d-flex flex-column align-items-center">
              <Form.Group
                className="mb-3 col-md-6 col-12"
                controlId="formPassword"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group
                className="mb-3 col-md-6 col-12"
                controlId="formRePassword"
              >
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Re-Enter Password"
                  value={rePassword}
                  onChange={(event) => setRePassword(event.target.value)}
                  required
                />
                {mobileNoValidation === 'invalid' ? (
                  <div className="invalid-feedback">Password Not Matched!</div>
                ) : (
                  <>
                    <div className="valid-feedback">Looking Good !</div>
                  </>
                )}
              </Form.Group>
            </div>

            <Modal.Footer className="d-flex justify-content-center">
              <Button
                className="col-5 mt-3"
                variant="primary"
                type="submit"
                disabled={disable}
              >
                Register
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default RegisterModal;
